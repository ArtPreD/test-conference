# Conference management API
This application allows:
- Create and cancel conferences
- Get all conferences or a particular by id
- Check availability of the conference based on available seats in a room
- Add and remove users from the conference
- Get conference participants  

It is also possible to create and update a user, and to get all the users or a particular one by id.

To build and run the project, use the following command:  
`mvn spring-boot:run`  
This will started the embedded server on port 8080. To specify different port, for example 8085, use this command:   
`mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=8085`  
Make sure that `JAVA_HOME` points to jdk8.

To access the application, visit `http://localhost:<server.port>`.  
To access Swagger UI, visit `http://localhost:<server.port>/swagger-ui/index.html#/`

For using Conference API you need to login first.

When the project is launched, the following users will be created automatically:
 - `1. Name: Test Member 1, Username: TestMember1, Password: 1, Role: MEMBER`
 - `2. Name: Test Member 2, Username: TestMember2, Password: 1, Role: MEMBER`
 - `3. Name: Test Guest 1, Username: TestGuest1, Password: 1, Role: GUEST`
 - `4. Name: Test Guest 2, Username: TestGuest2, Password: 1, Role: GUEST`
 - `5. Name: Test Guest 3, Username: TestGuest3, Password: 1, Role: GUEST`

You can also create new users using `http://localhost:<server.port>/api/users/save` endpoint.

So for first you should send POST request to `http://localhost:<server.port>/auth/signin` with a body, for example:
>{"username": "TestMember1", "password": "1" }

The "Authorization" parameter in header will come in the response

>Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJUZXN0TWVtYmVyMSIsInJvbGVzIjpbIk1FTUJFUiJdLCJmdWxsTmFtZSI6IlRlc3QgTWVtYmVyIDEiLCJpZCI6MSwiaWF0IjoxNjc1OTUzNzc3LCJleHAiOjE2NzU5NTQ2Nzd9.o5PFDfFFVu9HgtD6DeQmK9wV0oR9dWUFbWMzWGMBnlo

You need to copy the token after "Bearer" and use it in swagger-ui for authorized requests.