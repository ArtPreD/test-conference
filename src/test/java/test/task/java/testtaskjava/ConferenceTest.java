package test.task.java.testtaskjava;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.response.ValidatableResponse;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import test.task.java.testtaskjava.constant.TestConstants;

import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConferenceTest {

    @LocalServerPort
    private int port;

    @Test
    void createConference() {
        Response auth = sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_1);
        String authorization = auth.header(TestConstants.AUTHORIZATION_HEADER);

        final int capacity = 10;
        final String name = "Test Conference CREATE";

        ValidatableResponse validatableResponse =
                sendRequestToCreateTestConference(authorization, name, capacity)
                        .then().statusCode(200);

        validatableResponse.body("id", Matchers.greaterThan(0));
        validatableResponse.body("name", Matchers.equalTo(name));
        validatableResponse.body("capacity", Matchers.equalTo(capacity));
        validatableResponse.body("available", Matchers.equalTo(capacity));
        validatableResponse.body("open", Matchers.is(TestConstants.ENABLED_TEST));
    }

    @Test
    public void cancelConference() {
        // Login in as Member 1
        Response authMember1 = sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_1);
        String authorizationMember1 = authMember1.header(TestConstants.AUTHORIZATION_HEADER);

        // Login in as Member 2
        Response authMember2 = sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_2);
        String authorizationMember2 = authMember2.header(TestConstants.AUTHORIZATION_HEADER);

        // Member 1 created a conference
        Response response =
                sendRequestToCreateTestConference(authorizationMember1, "Test Conference PERMIT", 10);

        // Get conference id from response
        ResponseBodyExtractionOptions body = response.body();
        int conferenceId = body.jsonPath().get("id");

        // Trying to cancel conference by Member 2
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember2)
                .put("/api/conference/cancel/" + conferenceId).then().statusCode(403);

        // Only creator can be cancel conference
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .put("/api/conference/cancel/" + conferenceId).then().statusCode(200);
    }

    @Test
    public void checkAvailabilityConference() {
        // Login in as Member 1
        Response authMember1 = sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_1);
        String authorizationMember1 = authMember1.header(TestConstants.AUTHORIZATION_HEADER);

        // Member 1 created a conference
        Response response =
                sendRequestToCreateTestConference(authorizationMember1, "Test Conference AVAILABILITY", 2);

        // Get conference id from response
        ResponseBodyExtractionOptions body = response.body();
        int conferenceId = body.jsonPath().get("id");

        // Check availability
        ValidatableResponse firstCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is available. 2 places are remaining.
        firstCheckResponse.body("status", Matchers.is(TestConstants.ENABLED_TEST));

        // Add first guest to the conference
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"username\": \"TestGuest1\"\n" +
                        "}")
                .when()
                .post("/api/conference/add/" + conferenceId);

        // Second check availability
        ValidatableResponse secondCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is available. 1 places are remaining.
        secondCheckResponse.body("status", Matchers.is(TestConstants.ENABLED_TEST));

        // Add second guest to the conference
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"username\": \"TestGuest2\"\n" +
                        "}")
                .when().post("/api/conference/add/" + conferenceId);

        // Third check availability
        ValidatableResponse thirdCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is unavailable. 0 places are remaining.
        thirdCheckResponse.body("status", Matchers.is(TestConstants.DISABLED_TEST));
    }

    @Test
    public void addUserToConference() {
        // Login in as Member 1
        Response authMember1 = sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_1);
        String authorizationMember1 = authMember1.header(TestConstants.AUTHORIZATION_HEADER);

        // Member 1 created a conference
        Response response =
                sendRequestToCreateTestConference(authorizationMember1, "Test Conference ADD", 1);

        // Get conference id from response
        ResponseBodyExtractionOptions body = response.body();
        int conferenceId = body.jsonPath().get("id");

        // Check availability
        ValidatableResponse firstCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is available. 1 place remaining.
        firstCheckResponse.body("status", Matchers.is(TestConstants.ENABLED_TEST));

        // Add guest to the conference
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"username\": \"TestGuest1\"\n" +
                        "}")
                .when()
                .post("/api/conference/add/" + conferenceId).then().statusCode(200);

        // Second check availability
        ValidatableResponse secondCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is unavailable. 0 place remaining.
        secondCheckResponse.body("status", Matchers.is(TestConstants.DISABLED_TEST));

        // Trying to add one more guest
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"username\": \"TestGuest2\"\n" +
                        "}")
                .when()
                .post("/api/conference/add/" + conferenceId).then().statusCode(400);
    }

    @Test
    public void removeUserFromConference() {
        // Login in as Member 1
        Response authMember1 = sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_1);
        String authorizationMember1 = authMember1.header(TestConstants.AUTHORIZATION_HEADER);

        // Member 1 created a conference
        Response response =
                sendRequestToCreateTestConference(authorizationMember1, "Test Conference REMOVE", 1);

        // Get conference id from response
        ResponseBodyExtractionOptions body = response.body();
        int conferenceId = body.jsonPath().get("id");

        // Check availability
        ValidatableResponse firstCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is available. 1 place remaining.
        firstCheckResponse.body("status", Matchers.is(TestConstants.ENABLED_TEST));

        // Add guest to the conference
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"username\": \"TestGuest1\"\n" +
                        "}")
                .when()
                .post("/api/conference/add/" + conferenceId);

        // Second check availability
        ValidatableResponse secondCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is unavailable. 0 place remaining.
        secondCheckResponse.body("status", Matchers.is(TestConstants.DISABLED_TEST));

        // Remove user from conference
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"username\": \"TestGuest1\"\n" +
                        "}")
                .when()
                .post("/api/conference/remove/" + conferenceId);

        // Third check availability
        ValidatableResponse thirdCheckResponse =
                checkConferenceAvailability(authorizationMember1, conferenceId).then().statusCode(200);

        // Conference is available. 1 places are remaining.
        thirdCheckResponse.body("status", Matchers.is(TestConstants.ENABLED_TEST));
    }

    @Test
    public void getAllConference() {
        // Login in as Member 1
        Response authMember1 = sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_1);
        String authorizationMember1 = authMember1.header(TestConstants.AUTHORIZATION_HEADER);

        // Get all conferences
        ValidatableResponse validatableResponse = given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorizationMember1)
                .accept(ContentType.JSON)
                .get("/api/conference/all").then().statusCode(200);

        validatableResponse.body("content", Matchers.notNullValue());
    }

    private Response sendAuthenticationRequest(String jsonCredentials) {
        return given().port(port)
                .contentType(ContentType.JSON)
                .body(jsonCredentials)
                .when().post("/auth/signin");
    }

    private Response sendRequestToCreateTestConference(String authorization, String name, int capacity) {
        return given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorization)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"" + name + "\",\n" +
                        "    \"capacity\": \n" + capacity + "}")
                .when().post("/api/conference/save");
    }

    private Response checkConferenceAvailability(String authorization, long conferenceId) {
        return given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorization)
                .get("/api/conference/check/" + conferenceId);
    }


}
