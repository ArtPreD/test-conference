package test.task.java.testtaskjava;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.response.ValidatableResponse;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import test.task.java.testtaskjava.constant.TestConstants;

import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthorizationTests {

    @LocalServerPort
    private int port;

    @Test
    void loginIn() {
        ValidatableResponse validatableResponse =
                sendAuthenticationRequest(TestConstants.CREDENTIALS_MEMBER_1)
                        .then().statusCode(200);
        validatableResponse.header(TestConstants.AUTHORIZATION_HEADER, Matchers.notNullValue());
        validatableResponse.header(TestConstants.REFRESH_TOKEN_HEADER, Matchers.notNullValue());
    }

    @Test
    void signUp() {
        ValidatableResponse validatableResponse =
                sendRequestToCreateNewUser()
                .then().statusCode(200);

        validatableResponse.body("id", Matchers.greaterThan(0));
        validatableResponse.body("name", Matchers.equalTo(TestConstants.NAME_TEST));
        validatableResponse.body("username", Matchers.equalTo(TestConstants.USERNAME_TEST));
        validatableResponse.body("role", Matchers.equalTo(TestConstants.ROLE_TEST));
        validatableResponse.body("enabled", Matchers.is(TestConstants.ENABLED_TEST));

        // Login in as TestMember1
        Response response = sendAuthenticationRequest(
                "{\n" +
                "  \"username\": \"TestMember1\",\n" +
                "  \"password\": \"1\"\n" +
                "}");

        String authorization = response.header(TestConstants.AUTHORIZATION_HEADER);

        ResponseBodyExtractionOptions body = validatableResponse.extract().body();
        int id = body.jsonPath().get("id");

        // Delete created user
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorization)
                .when().delete("/api/users/delete/" + id);

    }

    @Test
    public void deleteUser() {
        // Create test user
        Response response = sendRequestToCreateNewUser();

        // Login in as TestMember1
        Response authResponse = sendAuthenticationRequest(
                "{\n" +
                        "  \"username\": \"TestMember1\",\n" +
                        "  \"password\": \"1\"\n" +
                        "}");

        String authorization = authResponse.header(TestConstants.AUTHORIZATION_HEADER);

        ResponseBodyExtractionOptions body = response.body();
        int id = body.jsonPath().get("id");

        // Delete created user
        given().port(port)
                .header(TestConstants.AUTHORIZATION_HEADER, authorization)
                .when().delete("/api/users/delete/" + id).then().statusCode(200);

    }

    private Response sendRequestToCreateNewUser() {
        return given().port(port)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\n" +
                        "  \"name\": \"" + TestConstants.NAME_TEST + "\",\n" +
                        "  \"username\": \"" + TestConstants.USERNAME_TEST + "\",\n" +
                        "  \"role\": \"" + TestConstants.ROLE_TEST + "\",\n" +
                        "  \"password\": \"" + TestConstants.PASSWORD_TEST + "\",\n" +
                        "  \"confirmPassword\": \"" + TestConstants.CONFIRM_PASSWORD_TEST + "\"\n" +
                        "}")
                .when().post("/api/users/save");
    }

    private Response sendAuthenticationRequest(String jsonCredentials) {
        return given().port(port)
                .contentType(ContentType.JSON)
                .body(jsonCredentials)
                .when().post("/auth/signin");
    }

}
