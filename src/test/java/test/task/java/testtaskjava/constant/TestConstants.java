package test.task.java.testtaskjava.constant;

public final class TestConstants {

   public static final String CREDENTIALS_MEMBER_1 = "{\n" +
            "    \"username\": \"TestMember1\",\n" +
            "    \"password\": \"1\"\n" +
            "}";

   public static final String CREDENTIALS_MEMBER_2 = "{\n" +
           "    \"username\": \"TestMember2\",\n" +
           "    \"password\": \"1\"\n" +
           "}";

   public static final String NAME_TEST = "Test Member 3";
   public static final String USERNAME_TEST = "TestMember3";
   public static final String ROLE_TEST = "MEMBER";
   public static final boolean ENABLED_TEST = true;
   public static final boolean DISABLED_TEST = false;
   public static final String PASSWORD_TEST = "1";
   public static final String CONFIRM_PASSWORD_TEST = "1";

   public static final String AUTHORIZATION_HEADER = "Authorization";
   public static final String REFRESH_TOKEN_HEADER = "Refresh-Token";

    private TestConstants() {
    }
}
