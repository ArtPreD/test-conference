package test.task.java.testtaskjava.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import test.task.java.testtaskjava.controller.handler.UserHandlerMethodArgumentResolver;

import java.util.List;

@Configuration
public class ApplicationConfig implements WebMvcConfigurer {

    private final UserHandlerMethodArgumentResolver userHandlerMethodArgumentResolver;

    @Autowired
    public ApplicationConfig(UserHandlerMethodArgumentResolver userHandlerMethodArgumentResolver) {
        this.userHandlerMethodArgumentResolver = userHandlerMethodArgumentResolver;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(userHandlerMethodArgumentResolver);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
