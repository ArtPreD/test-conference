package test.task.java.testtaskjava.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.task.java.testtaskjava.entity.Conference;
import test.task.java.testtaskjava.mapper.MapperService;
import test.task.java.testtaskjava.model.ConferenceModel;
import test.task.java.testtaskjava.security.mapper.UserMapperService;

import java.util.stream.Collectors;

@Service
public class ConferenceMapperService implements MapperService<Conference, ConferenceModel> {

    private final ModelMapper modelMapper;
    private final UserMapperService userMapperService;

    @Autowired
    public ConferenceMapperService(ModelMapper modelMapper, UserMapperService userMapperService) {
        this.modelMapper = modelMapper;
        this.userMapperService = userMapperService;
    }

    @Override
    public Conference convertFromModel(ConferenceModel model) {
        return modelMapper.typeMap(ConferenceModel.class, Conference.class).map(model);
    }

    @Override
    public ConferenceModel convertToModel(Conference entity) {
        return modelMapper.typeMap(Conference.class, ConferenceModel.class).addMappings(mapping -> {
            mapping.map(source -> entity.isOpen() ? entity.getCapacity() - entity.getUsers().size() : 0, ConferenceModel::setAvailable);
            mapping.map(source -> entity.getUsers().stream().map(userMapperService::convertToModel).collect(Collectors.toList()), ConferenceModel::setMembers);
        }).map(entity);
    }

}