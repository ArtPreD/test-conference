package test.task.java.testtaskjava.mapper;

public interface MapperService<M, D> {

    M convertFromModel(D model);

    D convertToModel(M entity);

}
