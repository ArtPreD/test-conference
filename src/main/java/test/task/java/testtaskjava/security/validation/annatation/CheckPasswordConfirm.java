package test.task.java.testtaskjava.security.validation.annatation;

import test.task.java.testtaskjava.security.validation.PasswordConfirmValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = {PasswordConfirmValidator.class})
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckPasswordConfirm {

    String message() default "{error.password.confirm}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };

}
