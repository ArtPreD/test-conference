package test.task.java.testtaskjava.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException)
            throws IOException, ServletException {
        log.error("Jwt authentication failed:" + authException);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> errorData = new HashMap<>();
        errorData.put("error", HttpStatus.UNAUTHORIZED.getReasonPhrase());
        errorData.put("status_code", String.valueOf(HttpStatus.UNAUTHORIZED.value()));
        errorData.put("message", "You have provided invalid credentials or your token has expired");
        objectMapper.writeValue(response.getOutputStream(), errorData);
    }

}
