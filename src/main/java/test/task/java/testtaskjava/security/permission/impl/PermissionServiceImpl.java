package test.task.java.testtaskjava.security.permission.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import test.task.java.testtaskjava.entity.IEntity;
import test.task.java.testtaskjava.security.permission.PermissionService;

@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class PermissionServiceImpl implements PermissionService {

    private MutableAclService aclService;
    private PlatformTransactionManager platformTransactionManager;

    @Override
    public void addPermissionsForUser(IEntity target, String username, Permission... permissions) {
        Sid sid = new PrincipalSid(username);
        TransactionTemplate transactionTemplate = new TransactionTemplate(platformTransactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                ObjectIdentity objectIdentity = new ObjectIdentityImpl(target.getClass(), target.getId());
                MutableAcl acl = null;
                try {
                    acl = (MutableAcl) aclService.readAclById(objectIdentity);
                } catch (NotFoundException e) {
                    acl = aclService.createAcl(objectIdentity);
                }
                for (Permission permission : permissions) {
                    acl.insertAce(acl.getEntries().size(), permission, sid, true);
                }
                aclService.updateAcl(acl);
            }
        });
    }

}
