package test.task.java.testtaskjava.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import test.task.java.testtaskjava.security.model.UserModel;
import test.task.java.testtaskjava.security.service.UserService;
import test.task.java.testtaskjava.security.validation.annatation.order.ValidationSequence;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("save")
    public UserModel createUser(@Validated(ValidationSequence.class) @RequestBody UserModel model) {
        return userService.saveUser(model);
    }

    @DeleteMapping("delete/{id}")
    public void deleteUser(@PathVariable(name = "id") long userId) {
        userService.deleteUser(userId);
    }

}
