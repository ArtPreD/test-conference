package test.task.java.testtaskjava.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.task.java.testtaskjava.security.entity.RefreshToken;
import test.task.java.testtaskjava.security.entity.User;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    @Override
    Optional<RefreshToken> findById(Long id);

    Optional<RefreshToken> findByToken(String token);

    Optional<RefreshToken> findByUser(User user);

}
