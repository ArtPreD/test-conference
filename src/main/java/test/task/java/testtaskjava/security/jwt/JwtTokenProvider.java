package test.task.java.testtaskjava.security.jwt;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import test.task.java.testtaskjava.security.config.JwtProperties;
import test.task.java.testtaskjava.security.entity.RefreshToken;
import test.task.java.testtaskjava.security.entity.User;
import test.task.java.testtaskjava.security.role.Role;
import test.task.java.testtaskjava.security.service.RefreshTokenService;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Component
public class  JwtTokenProvider {

    private final JwtProperties jwtProperties;
    private final RefreshTokenService refreshTokenService;

    private String secretKey;

    @Autowired
    public JwtTokenProvider(JwtProperties jwtProperties, RefreshTokenService refreshTokenService) {
        this.jwtProperties = jwtProperties;
        this.refreshTokenService = refreshTokenService;
    }

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(jwtProperties.getSecretKey().getBytes());
    }

    public String createToken(User user) {
        Claims claims = Jwts.claims().setSubject(user.getUsername());
        Role[] roles = new Role[] { user.getRole() };
        claims.put("roles", Arrays.asList(roles));
        claims.put("fullName", user.getName());
        claims.put("id", user.getId());

        Date now = new Date();
        Date validity = new Date(now.getTime() + jwtProperties.getValidityInMs());

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public RefreshToken doGenerateRefreshToken(long userId) {
       return refreshTokenService.createRefreshToken(userId);
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(HttpHeaders.AUTHORIZATION);
        if (!StringUtils.isEmpty(bearerToken) && bearerToken.startsWith(jwtProperties.getTokenPrefix())) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenService.findByToken(token);
    }

    public RefreshToken verifyExpiration (RefreshToken token) {
        return refreshTokenService.verifyExpiration(token);
    }

}
