package test.task.java.testtaskjava.security.validation.annatation.order;

import javax.validation.GroupSequence;
import javax.validation.groups.Default;

@GroupSequence(value = { Default.class, First.class, Second.class })
public interface ValidationSequence {
}
