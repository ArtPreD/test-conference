package test.task.java.testtaskjava.security.role;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    MEMBER,
    GUEST;

    public static boolean contains(String value) {
        for (Role roleEnum : values()) {
            if(roleEnum.name().equalsIgnoreCase(value)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String getAuthority() {
        return this.name();
    }

}
