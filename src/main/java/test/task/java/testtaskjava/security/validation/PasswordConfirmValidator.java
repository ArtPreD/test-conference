package test.task.java.testtaskjava.security.validation;

import org.springframework.stereotype.Component;
import test.task.java.testtaskjava.security.model.UserModel;
import test.task.java.testtaskjava.security.validation.annatation.CheckPasswordConfirm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class PasswordConfirmValidator implements ConstraintValidator<CheckPasswordConfirm, UserModel> {

    @Override
    public boolean isValid(UserModel model, ConstraintValidatorContext context) {
        return model.getPassword().equals(model.getConfirmPassword());
    }

}
