package test.task.java.testtaskjava.security.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import test.task.java.testtaskjava.security.model.UserModel;

public interface UserService extends UserDetailsService {

    UserModel saveUser(UserModel model);

    void deleteUser(long userId);

}
