package test.task.java.testtaskjava.security.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import test.task.java.testtaskjava.mapper.MapperService;
import test.task.java.testtaskjava.security.entity.User;
import test.task.java.testtaskjava.security.model.UserModel;
import test.task.java.testtaskjava.util.ParseUtil;

@Service
public class UserMapperService implements MapperService<User, UserModel> {

    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapperService(ModelMapper modelMapper, @Lazy PasswordEncoder passwordEncoder) {
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User convertFromModel(UserModel model) {
        return modelMapper.typeMap(UserModel.class, User.class).addMappings(mapping -> {
            mapping.map(source -> passwordEncoder.encode(model.getPassword()), User::setPassword);
            mapping.map(source -> ParseUtil.parseRole(model.getRole()), User::setRole);
        }).map(model);
    }

    @Override
    public UserModel convertToModel(User entity) {
        return modelMapper.typeMap(User.class, UserModel.class).addMappings(mapping -> {
            mapping.skip(UserModel::setPassword);
        }).map(entity);
    }

}
