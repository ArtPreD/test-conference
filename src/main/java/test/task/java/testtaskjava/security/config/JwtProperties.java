package test.task.java.testtaskjava.security.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JwtProperties {

    @Value("${application.jwt.secret:k.XY!qV%v;o(/q5{tzd`@,creLBUL!}")
    private String secretKey;

    @Value("${application.jwt.validity:#{1000*60*15}}")
    private long validityInMs;

    @Value("${application.jwt.validity:#{60*60*24*30}}")
    private long refreshValidityInS;

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public long getValidityInMs() {
        return validityInMs;
    }

    public void setValidityInMs(long validityInMs) {
        this.validityInMs = validityInMs;
    }

    public long getRefreshValidityInS() {
        return refreshValidityInS;
    }

    public void setRefreshValidityInMs(long refreshValidityInS) {
        this.refreshValidityInS = refreshValidityInS;
    }

    public String getTokenPrefix() {
        return "Bearer ";
    }

}
