package test.task.java.testtaskjava.security.permission;

import org.springframework.security.acls.model.Permission;
import test.task.java.testtaskjava.entity.IEntity;

public interface PermissionService {

    void addPermissionsForUser(IEntity target, String username, Permission... permissions);

}
