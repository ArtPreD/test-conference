package test.task.java.testtaskjava.security.model;

import lombok.Data;

@Data
public class UsernameAndPasswordAuthenticationRequest {

    private String username;
    private String password;

}
