package test.task.java.testtaskjava.security.validation;

import org.springframework.stereotype.Component;
import test.task.java.testtaskjava.security.model.UserModel;
import test.task.java.testtaskjava.security.validation.annatation.CheckPasswordRequired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class PasswordRequireValidator implements ConstraintValidator<CheckPasswordRequired, UserModel> {

    @Override
    public boolean isValid(UserModel model, ConstraintValidatorContext context) {
        return !("".equals(model.getPassword()) && "".equals(model.getConfirmPassword()));
    }

}
