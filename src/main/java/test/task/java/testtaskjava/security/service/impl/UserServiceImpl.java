package test.task.java.testtaskjava.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.task.java.testtaskjava.entity.Conference;
import test.task.java.testtaskjava.exception.UserNotFoundException;
import test.task.java.testtaskjava.security.entity.User;
import test.task.java.testtaskjava.security.mapper.UserMapperService;
import test.task.java.testtaskjava.security.model.UserModel;
import test.task.java.testtaskjava.security.repository.UserRepository;
import test.task.java.testtaskjava.security.service.RefreshTokenService;
import test.task.java.testtaskjava.security.service.UserService;

import java.util.Optional;
import java.util.Set;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapperService userMapperService;
    private final RefreshTokenService refreshTokenService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserMapperService userMapperService, RefreshTokenService refreshTokenService) {
        this.userRepository = userRepository;
        this.userMapperService = userMapperService;
        this.refreshTokenService = refreshTokenService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findDistinctByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format("Username %s not found", username))
                );
    }

    @Override
    @Transactional
    public UserModel saveUser(UserModel model) {
        User user = userMapperService.convertFromModel(model);
        user.setEnabled(true);

        User save = userRepository.save(user);

        return userMapperService.convertToModel(save);
    }

    @Override
    @Transactional
    public void deleteUser(long userId) {
        Optional<User> byId = userRepository.findById(userId);
        if (byId.isPresent()) {
            User user = byId.get();
            Set<Conference> conferences = user.getConferences();
            conferences.clear();
            user = userRepository.save(user);
            refreshTokenService.deleteRefreshTokenByUser(user);
            userRepository.delete(user);
        } else {
            throw new UserNotFoundException(String.format("User with id %s not found", userId));
        }
    }
}
