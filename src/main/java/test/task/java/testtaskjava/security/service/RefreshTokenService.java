package test.task.java.testtaskjava.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.task.java.testtaskjava.exception.TokenRefreshException;
import test.task.java.testtaskjava.exception.UserNotFoundException;
import test.task.java.testtaskjava.security.config.JwtProperties;
import test.task.java.testtaskjava.security.entity.RefreshToken;
import test.task.java.testtaskjava.security.entity.User;
import test.task.java.testtaskjava.security.repository.RefreshTokenRepository;
import test.task.java.testtaskjava.security.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {

    private final JwtProperties jwtProperties;

    private final RefreshTokenRepository refreshTokenRepository;

    private final UserRepository userRepository;

    @Autowired
    public RefreshTokenService(JwtProperties jwtProperties,
                               RefreshTokenRepository refreshTokenRepository, UserRepository userRepository) {
        this.jwtProperties = jwtProperties;
        this.refreshTokenRepository = refreshTokenRepository;
        this.userRepository = userRepository;
    }

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken createRefreshToken(Long userId) {
        Optional<User> byId = userRepository.findById(userId);
        if (byId.isPresent()) {
            User user = byId.get();
            Optional<RefreshToken> refreshTokenByUser = refreshTokenRepository.findByUser(user);
            if (refreshTokenByUser.isPresent()) {
                RefreshToken refreshToken = refreshTokenByUser.get();
                return verifyExpiration(refreshToken);
            } else {
                RefreshToken refreshToken = new RefreshToken();
                refreshToken.setUser(user);
                refreshToken.setExpiryDate(LocalDateTime.now().plusSeconds(jwtProperties.getRefreshValidityInS()));
                refreshToken.setToken(UUID.randomUUID().toString());
                refreshToken = refreshTokenRepository.save(refreshToken);
                return refreshToken;
            }
        } else {
            throw new UserNotFoundException("Cannot find user with id " + userId);
        }
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(LocalDateTime.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new TokenRefreshException(token.getToken(), "Refresh token was expired. Please make a new signin request");
        }
        return token;
    }

    public void deleteRefreshTokenByUser(User user) {
        Optional<RefreshToken> byUser = refreshTokenRepository.findByUser(user);
        if (byUser.isPresent()) {
            RefreshToken refreshToken = byUser.get();
            refreshTokenRepository.delete(refreshToken);
        }
    }

}
