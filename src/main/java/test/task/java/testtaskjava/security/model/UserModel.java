package test.task.java.testtaskjava.security.model;

import lombok.Getter;
import lombok.Setter;
import test.task.java.testtaskjava.security.validation.annatation.CheckPasswordConfirm;
import test.task.java.testtaskjava.security.validation.annatation.CheckPasswordRequired;
import test.task.java.testtaskjava.security.validation.annatation.order.First;
import test.task.java.testtaskjava.security.validation.annatation.order.Second;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@CheckPasswordRequired(message = "Password and Confirm Password are required fields", groups = First.class)
@CheckPasswordConfirm(message = "Passwords don't match", groups = Second.class)
public class UserModel {

    private long id;
    @NotEmpty(message = "Username is required field")
    private String username;
    @NotEmpty(message = "Name is required field")
    private String name;
    private String password;
    private String confirmPassword;
    @NotEmpty(message = "Role is required field")
    private String role;

    private long expiresAt;
    private String token;
    private boolean enabled;

}

