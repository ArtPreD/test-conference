package test.task.java.testtaskjava.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.task.java.testtaskjava.exception.TokenRefreshException;
import test.task.java.testtaskjava.security.config.JwtProperties;
import test.task.java.testtaskjava.security.entity.RefreshToken;
import test.task.java.testtaskjava.security.jwt.JwtTokenProvider;
import test.task.java.testtaskjava.security.model.TokenRefreshRequest;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final JwtTokenProvider jwtTokenProvider;
    private final JwtProperties jwtProperties;

    @Autowired
    public AuthController(JwtTokenProvider jwtTokenProvider, JwtProperties jwtProperties) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.jwtProperties = jwtProperties;
    }

    @PostMapping("/refreshtoken")
    public void refreshtoken(@Valid @RequestBody TokenRefreshRequest request, HttpServletResponse response) {
        String requestRefreshToken = request.getRefreshToken();
        Optional<RefreshToken> byToken = jwtTokenProvider.findByToken(requestRefreshToken);
        if (byToken.isPresent()) {
            RefreshToken refreshToken = byToken.get();
            jwtTokenProvider.verifyExpiration(refreshToken);

            String token = jwtTokenProvider.createToken(refreshToken.getUser());

            response.addHeader(HttpHeaders.AUTHORIZATION, jwtProperties.getTokenPrefix() + token);
        } else {
            throw  new TokenRefreshException(requestRefreshToken, "Refresh token is not in database!");
        }
    }

}
