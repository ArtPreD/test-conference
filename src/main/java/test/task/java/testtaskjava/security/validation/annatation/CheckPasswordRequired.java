package test.task.java.testtaskjava.security.validation.annatation;

import test.task.java.testtaskjava.security.validation.PasswordRequireValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = {PasswordRequireValidator.class})
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckPasswordRequired {

    String message() default "{error.password.required}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };

}
