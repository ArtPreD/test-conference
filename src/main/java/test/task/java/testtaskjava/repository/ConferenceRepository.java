package test.task.java.testtaskjava.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import test.task.java.testtaskjava.entity.Conference;

import javax.persistence.LockModeType;
import java.util.Optional;

@Repository
public interface ConferenceRepository extends PagingAndSortingRepository<Conference, Long> {

    Page<Conference> findAll(Pageable pageable);

    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<Conference> findWithLockingById(long id);

}
