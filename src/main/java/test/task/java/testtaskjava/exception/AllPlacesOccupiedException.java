package test.task.java.testtaskjava.exception;

public class AllPlacesOccupiedException extends RuntimeException {

    public AllPlacesOccupiedException() {
    }

    public AllPlacesOccupiedException(String message) {
        super(message);
    }

}
