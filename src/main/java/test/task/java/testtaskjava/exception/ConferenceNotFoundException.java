package test.task.java.testtaskjava.exception;

public class ConferenceNotFoundException extends RuntimeException {

    public ConferenceNotFoundException() {
    }

    public ConferenceNotFoundException(String message) {
        super(message);
    }

}
