package test.task.java.testtaskjava.exception;

public class PasswordDidNotMatchException extends RuntimeException {

    public PasswordDidNotMatchException() { }

    public PasswordDidNotMatchException(String message) {
        super(message);
    }

}
