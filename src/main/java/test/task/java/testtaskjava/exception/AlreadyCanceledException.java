package test.task.java.testtaskjava.exception;

public class AlreadyCanceledException extends RuntimeException {

    public AlreadyCanceledException() {
    }

    public AlreadyCanceledException(String message) {
        super(message);
    }

}
