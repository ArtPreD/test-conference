package test.task.java.testtaskjava.exception;

public class UserNotInConferenceException extends RuntimeException {

    public UserNotInConferenceException() {
    }

    public UserNotInConferenceException(String message) {
        super(message);
    }

}
