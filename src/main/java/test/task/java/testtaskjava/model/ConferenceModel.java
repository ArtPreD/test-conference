package test.task.java.testtaskjava.model;

import lombok.Data;
import test.task.java.testtaskjava.security.model.UserModel;

import java.util.List;

@Data
public class ConferenceModel {

    private Long id;
    private String name;
    private int capacity;
    private int available;
    private boolean open;

    private List<UserModel> members;

}

