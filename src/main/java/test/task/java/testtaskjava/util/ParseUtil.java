package test.task.java.testtaskjava.util;

import test.task.java.testtaskjava.exception.ParseException;
import test.task.java.testtaskjava.security.role.Role;

public class ParseUtil {

    public static Role parseRole(String role) {
        if (Role.contains(role)) {
            return Role.valueOf(role.toUpperCase());
        } else {
            throw new ParseException(String.format("Cannot parse role %s. This role doesn't exist in enumeration", role));
        }
    }

}
