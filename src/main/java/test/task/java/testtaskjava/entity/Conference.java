package test.task.java.testtaskjava.entity;

import lombok.Getter;
import lombok.Setter;
import test.task.java.testtaskjava.security.entity.User;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "conference")
public class Conference implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conf_seq")
    @SequenceGenerator(name = "conf_seq", sequenceName = "conf_seq", allocationSize = 1)
    @Column(updatable = false, nullable = false)
    private Long id;

    private String name;

    private int capacity;

    private boolean open;

    @ManyToMany
    @JoinTable(
            name = "conference_users",
            joinColumns = @JoinColumn(name = "conference_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> users = new HashSet<>();

    @Override
    public String toString() {
        return "Conference{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", capacity=" + capacity +
                '}';
    }

}
