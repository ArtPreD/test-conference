package test.task.java.testtaskjava.controller.model;

import lombok.Data;

@Data
public class CheckModel {

    private boolean status;

    public CheckModel(boolean status) {
        this.status = status;
    }

}
