package test.task.java.testtaskjava.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import test.task.java.testtaskjava.controller.model.CheckModel;
import test.task.java.testtaskjava.model.ConferenceModel;
import test.task.java.testtaskjava.security.entity.User;
import test.task.java.testtaskjava.service.ConferenceService;

import java.security.Principal;

import static test.task.java.testtaskjava.controller.swagger.SwaggerConstants.*;

@RestController
@RequestMapping("/api/conference")
public class ConferenceController {

    private final ConferenceService conferenceService;

    public ConferenceController(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
    }

    @GetMapping("all")
    @Operation(
            tags = CONFERENCE_CONTROLLER_TAG,
            summary = "Get all conferences",
            description = "Important. Don't forget to specify sorting. For example by name",
            responses = {
                    @ApiResponse(
                            responseCode = STATUS_OK,
                            content = {
                                    @Content(
                                            mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceModel.class), examples = {
                                            @ExampleObject(CONFERENCE_RESPONSE_BODY_EXAMPLE)
                                    })
                            })
            }
    )
    @SecurityRequirement(name = "Bearer Authentication")
    public Page<ConferenceModel> getAllConferences (
            @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        return conferenceService.getAll(pageable);
    }

    @GetMapping("check/{id}")
    @Operation(
            tags = CONFERENCE_CONTROLLER_TAG,
            summary = "Is conference available for registration",
            description = "The conference is available for registration if there are enough seats in the room",
            parameters = {@Parameter(name = "conferenceId", description = "Conference Id")},
            responses = {
                    @ApiResponse(
                            responseCode = STATUS_OK, description = "Availability result",
                            content = {
                                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = CheckModel.class),  examples = {
                                            @ExampleObject(name = "Conference is available for registration", value = CONFERENCE_AVAILABLE_EXAMPLE),
                                            @ExampleObject(name = "Conference is unavailable for registration", value = CONFERENCE_UNAVAILABLE_EXAMPLE),
                                    })
                            }),
                    @ApiResponse(responseCode = STATUS_NOT_FOUND, description = "Conference was not found", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                            @ExampleObject(value = "{\"errorMessage\":\"Conference with id 0 was not found\"}")
                    }))
            }
    )
    @SecurityRequirement(name = "Bearer Authentication")
    public CheckModel checkAvailability (@PathVariable long id) {
        boolean availability = conferenceService.checkAvailability(id);
        return new CheckModel(availability);
    }

    @PutMapping("cancel/{id}")
    @Operation(
            tags = CONFERENCE_CONTROLLER_TAG,
            description = "IMPORTANT! To test this point, you need to be logged in as \"Member\" and have write permissions to this conference (be its owner)",
            summary = "Cancel conference",
            parameters = {@Parameter(name = "conferenceId", description = "Conference Id")},
            responses = {
                    @ApiResponse(
                            responseCode = STATUS_OK),
                    @ApiResponse(responseCode = STATUS_NOT_FOUND, description = RESOURCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                            @ExampleObject(name = "Conference was not found", value = "{\"errorMessage\":\"Conference with id 0 was not found\"}")
                    }))
            }
    )
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAuthority('MEMBER') and hasPermission(#id, 'test.task.java.testtaskjava.entity.Conference', 'WRITE')")
    public void cancelConference (@PathVariable long id) {
        conferenceService.cancelConference(id);
    }

    @PostMapping("save")
    @Operation(
            tags = CONFERENCE_CONTROLLER_TAG,
            summary = "Update conference",
            description = "IMPORTANT! To test this point, you need to be logged in as \"Member\"",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceModel.class), examples = {
                            @ExampleObject(CREATE_CONFERENCE_REQUEST_RESPONSE_BODY)
                    })
            }),
            responses = {
                    @ApiResponse(
                            responseCode = STATUS_CREATED, description = "Conference was created",
                            content = {
                                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceModel.class), examples = {
                                            @ExampleObject(CREATE_CONFERENCE_REQUEST_RESPONSE_BODY)
                                    })
                            })
            }
    )
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAuthority('MEMBER')")
    public ConferenceModel createConference (@RequestBody ConferenceModel model, Principal principal) {
        return conferenceService.create(model, principal);
    }

    @PostMapping("add/{id}")
    @Operation(
            tags = CONFERENCE_CONTROLLER_TAG,
            summary = "Add user to conference",
            parameters = {@Parameter(name = "conferenceId", description = "Conference Id"), @Parameter(name = "userId", description = "User Id")},
            responses = {
                    @ApiResponse(responseCode = STATUS_OK),
                    @ApiResponse(responseCode = STATUS_NOT_FOUND, description = RESOURCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                            @ExampleObject(name = "User was not found", value = "There is no user with this id"),
                            @ExampleObject(name = "Conference was not found", value = "Conference with id 0 does not exist")
                    })),
                    @ApiResponse(responseCode = STATUS_CONFLICT, description = "Conference adding failure", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                            @ExampleObject(name = "Conference was cancelled", value = "Conference with id 1 already close."),
                            @ExampleObject(name = "There are no seats available in the conference room", value = "All seats are occupied at the conference with id 1")
                    }))
            }
    )
    @SecurityRequirement(name = "Bearer Authentication")
    public void addUserToConference (@PathVariable(name = "id") long conferenceId, long userId) {
        conferenceService.addUserToConference(conferenceId, userId);
    }

    @PostMapping("remove/{id}")
    @Operation(
            tags = CONFERENCE_CONTROLLER_TAG,
            summary = "Remove user from conference",
            parameters = {@Parameter(name = "conferenceId", description = "Conference Id"), @Parameter(name = "user", schema = @Schema(implementation = User.class), description = "User. May contains only user id")},
            responses = {
                    @ApiResponse(responseCode = STATUS_OK),
                    @ApiResponse(responseCode = STATUS_NOT_FOUND, description = RESOURCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                            @ExampleObject(name = "User was not found", value = "There is no user with this id"),
                            @ExampleObject(name = "Conference was not found", value = "Conference with id 0 does not exist")
                    })),
                    @ApiResponse(responseCode = STATUS_CONFLICT, description = "Conference куьщмштп failure", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                            @ExampleObject(name = "Conference was cancelled", value = "Conference with id 1 already close.")
                    }))
            }
    )
    @SecurityRequirement(name = "Bearer Authentication")
    public void removeUserFromConference (@PathVariable(name = "id") long conferenceId, long userId) {
        conferenceService.removeUserFromConference(conferenceId, userId);
    }

}
