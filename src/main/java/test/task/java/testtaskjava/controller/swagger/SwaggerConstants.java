package test.task.java.testtaskjava.controller.swagger;

public final class SwaggerConstants {

    public static final String STATUS_OK = "200";
    public static final String STATUS_CREATED = "201";
    public static final String STATUS_NOT_FOUND = "404";
    public static final String STATUS_CONFLICT = "409";

    public static final String RESOURCE_WAS_NOT_FOUND_DESCRIPTION = "Resource was not found";
    public static final String CONFERENCE_CONTROLLER_TAG = "Conference Controller";

    public static final String CREATE_CONFERENCE_REQUEST_RESPONSE_BODY = "{\n \"id\": 1,\n \"name\": \"qwerty\",\n \"capacity\": 10,\n \"available\": 10,\n \"open\": true,\n \"members\": []\n}";
    public static final String CONFERENCE_AVAILABLE_EXAMPLE = "{\n \"status\": true\n}";
    public static final String CONFERENCE_UNAVAILABLE_EXAMPLE = "{\n \"status\": false\n}";
    public static final String CONFERENCE_RESPONSE_BODY_EXAMPLE = "{\n  \"content\": [\n    {\n      \"id\": 1,\n      \"name\": \"qwerty\",\n      \"capacity\": 10,\n      \"available\": 9,\n      \"open\": true,\n      \"members\": [\n        {\n          \"id\": 35,\n          \"username\": \"qwerty\",\n          \"name\": \"qwerty\",\n          \"password\": null,\n          \"confirmPassword\": null,\n          \"role\": \"GUEST\",\n          \"expiresAt\": 0,\n          \"token\": null,\n          \"enabled\": true\n        }\n      ]\n    }\n  ],\n  \"pageable\": {\n    \"sort\": {\n      \"sorted\": true,\n      \"unsorted\": false,\n      \"empty\": false\n    },\n    \"offset\": 0,\n    \"pageNumber\": 0,\n    \"pageSize\": 1,\n    \"unpaged\": false,\n    \"paged\": true\n" +"  },\n  \"totalPages\": 1,\n  \"totalElements\": 1,\n  \"last\": true,\n  \"size\": 1,\n  \"number\": 0,\n  \"sort\": {\n    \"sorted\": true,\n    \"unsorted\": false,\n    \"empty\": false\n  },\n  \"numberOfElements\": 1,\n  \"first\": true,\n  \"empty\": false\n}";


    private SwaggerConstants() {
    }
}
