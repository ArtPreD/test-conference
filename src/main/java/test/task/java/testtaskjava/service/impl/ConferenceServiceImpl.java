package test.task.java.testtaskjava.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.task.java.testtaskjava.entity.Conference;
import test.task.java.testtaskjava.exception.*;
import test.task.java.testtaskjava.mapper.impl.ConferenceMapperService;
import test.task.java.testtaskjava.model.ConferenceModel;
import test.task.java.testtaskjava.repository.ConferenceRepository;
import test.task.java.testtaskjava.security.entity.User;
import test.task.java.testtaskjava.security.permission.PermissionService;
import test.task.java.testtaskjava.security.repository.UserRepository;
import test.task.java.testtaskjava.service.ConferenceService;

import java.security.Principal;
import java.util.Optional;
import java.util.Set;

@Service
public class ConferenceServiceImpl implements ConferenceService {

    private final ConferenceRepository conferenceRepository;
    private final UserRepository userRepository;
    private final ConferenceMapperService mapperService;
    private final PermissionService permissionService;

    @Autowired
    public ConferenceServiceImpl(ConferenceRepository conferenceRepository, UserRepository userRepository,
                                 ConferenceMapperService mapperService, PermissionService permissionService) {
        this.conferenceRepository = conferenceRepository;
        this.userRepository = userRepository;
        this.mapperService = mapperService;
        this.permissionService = permissionService;
    }

    @Override
    public Page<ConferenceModel> getAll(Pageable pageable) {
        return conferenceRepository.findAll(pageable)
                .map(mapperService::convertToModel);
    }

    @Override
    @Transactional
    public boolean checkAvailability(long conferenceId) {
        Optional<Conference> byId = conferenceRepository.findWithLockingById(conferenceId);
        if (byId.isPresent()) {
            Conference conference = byId.get();
            try {
                checkConferenceAvailability(conference);
                return true;
            } catch (AllPlacesOccupiedException | AlreadyCanceledException exception) {
                return false;
            }
        } else {
            throw new ConferenceNotFoundException(String.format("Conference with id %s does not exist.", conferenceId));
        }
    }

    @Override
    @Transactional
    public void cancelConference(long conferenceId) {
        Optional<Conference> byId = conferenceRepository.findWithLockingById(conferenceId);
        if (byId.isPresent()) {
            Conference conference = byId.get();
            if (!conference.isOpen()) {
                throw new AlreadyCanceledException(String.format("Conference with id %s already close.", conferenceId));
            }
            conference.setOpen(false);
            Set<User> users = conference.getUsers();
            users.clear();
            conferenceRepository.save(conference);
        } else {
            throw new ConferenceNotFoundException(String.format("Conference with id %s does not exist.", conferenceId));
        }
    }

    @Override
    @Transactional
    public ConferenceModel create(ConferenceModel model, Principal principal) {
        Conference conference = mapperService.convertFromModel(model);
        conference.setOpen(true);
        Conference save = conferenceRepository.save(conference);

        Permission[] permissions = { BasePermission.READ, BasePermission.WRITE };
        permissionService.addPermissionsForUser(conference, principal.getName(), permissions);

        return mapperService.convertToModel(save);
    }

    @Override
    @Transactional
    public void addUserToConference(long conferenceId, long userId) {
        Optional<User> byId = userRepository.findById(userId);
        if (!byId.isPresent()) {
            throw new UserNotFoundException("There is no user with this id.");
        }
        Optional<Conference> withLockingById = conferenceRepository.findWithLockingById(conferenceId);

        if (withLockingById.isPresent()) {
            Conference conference = withLockingById.get();

            checkConferenceAvailability(conference);

            Set<User> users = conference.getUsers();
            users.add(byId.get());
            conferenceRepository.save(conference);
        } else {
            throw new ConferenceNotFoundException(String.format("Conference with id %s does not exist.", conferenceId));
        }
    }

    @Override
    @Transactional
    public void removeUserFromConference(long conferenceId, long userId) {
        Optional<User> byId = userRepository.findById(userId);
        if (!byId.isPresent()) {
            throw new UserNotFoundException("There is no user with this id or username.");
        }
        User user = byId.get();
        Optional<Conference> withLockingById = conferenceRepository.findWithLockingById(conferenceId);

        if (withLockingById.isPresent()) {
            Conference conference = withLockingById.get();

            if (conference.isOpen()) {
                Set<User> users = conference.getUsers();

                if (users.contains(user)) {
                    users.remove(user);
                    conferenceRepository.save(conference);
                } else {
                    throw new UserNotInConferenceException(String.format("User with id %s is not registered for this conference.", user.getId()));
                }
            } else {
                throw new AlreadyCanceledException(String.format("Conference with id %s already close.", conferenceId));
            }
        } else {
            throw new ConferenceNotFoundException(String.format("Conference with id %s does not exist.", conferenceId));
        }
    }

    private void checkConferenceAvailability(Conference conference) {
        if (conference.isOpen()) {
            Set<User> users = conference.getUsers();
            if (users.size() == conference.getCapacity()) {
                throw new AllPlacesOccupiedException(String.format("All seats are occupied at the conference with id %s", conference.getId()));
            }
        } else {
            throw new AlreadyCanceledException(String.format("Conference with id %s already close.", conference.getId()));
        }
    }

}
