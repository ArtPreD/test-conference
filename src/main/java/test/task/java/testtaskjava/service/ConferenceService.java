package test.task.java.testtaskjava.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import test.task.java.testtaskjava.model.ConferenceModel;
import test.task.java.testtaskjava.security.entity.User;

import java.security.Principal;

public interface ConferenceService {

    Page<ConferenceModel> getAll(Pageable pageable);

    boolean checkAvailability(long conferenceId);

    void cancelConference(long conferenceId);

    ConferenceModel create(ConferenceModel model, Principal principal);

    void addUserToConference(long conferenceId, long userId);

    void removeUserFromConference(long conferenceId, long userId);

}
