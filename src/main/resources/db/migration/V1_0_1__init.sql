create table CONFERENCE
(
    ID       BIGINT  not null
        primary key,
    CAPACITY INTEGER not null,
    NAME     VARCHAR(255),
    OPEN     BOOLEAN not null
);

create sequence CONF_SEQ;

create table USER
(
    ID       BIGINT  not null
        primary key,
    ENABLED  BOOLEAN not null,
    NAME     VARCHAR(255),
    PASSWORD VARCHAR(255),
    ROLE     VARCHAR(255),
    USERNAME VARCHAR(255)
);

create sequence USERS_SEQ;

create table CONFERENCE_USERS
(
    CONFERENCE_ID BIGINT not null,
    USER_ID       BIGINT not null,
    primary key (CONFERENCE_ID, USER_ID),
    constraint FKAFSYAXYCWDJJ91503AVW7FTS0
        foreign key (CONFERENCE_ID) references CONFERENCE (ID),
    constraint FKIJK7T6CD0V0JVQBQ68O4X3HNC
        foreign key (USER_ID) references USER (ID)
);

create table REFRESHTOKEN
(
    ID          BIGINT       not null
        primary key,
    EXPIRY_DATE TIMESTAMP    not null,
    TOKEN       VARCHAR(255) not null
        constraint UK_OR156WBNEYK8NOO4JSTV55II3
            unique,
    USER_ID     BIGINT,
    constraint FKFR75GE3IECDX26QE8AFH1SRF6
        foreign key (USER_ID) references USER (ID)
);

create sequence REFRESH_SEQ;

insert into USER(ID, ENABLED, NAME, PASSWORD, ROLE, USERNAME) values (USERS_SEQ.nextval, true, 'Test Member 1', '$2a$10$iE.0ldRzLk400H.AFXxlA.z3f2jRaudcPJzrQ80JjtGQSI6mhrNiu', 'MEMBER', 'TestMember1');
insert into USER(ID, ENABLED, NAME, PASSWORD, ROLE, USERNAME) values (USERS_SEQ.nextval, true, 'Test Member 2', '$2a$10$iE.0ldRzLk400H.AFXxlA.z3f2jRaudcPJzrQ80JjtGQSI6mhrNiu', 'MEMBER', 'TestMember2');
insert into USER(ID, ENABLED, NAME, PASSWORD, ROLE, USERNAME) values (USERS_SEQ.nextval, true, 'Test Guest 1', '$2a$10$iE.0ldRzLk400H.AFXxlA.z3f2jRaudcPJzrQ80JjtGQSI6mhrNiu', 'GUEST', 'TestGuest1');
insert into USER(ID, ENABLED, NAME, PASSWORD, ROLE, USERNAME) values (USERS_SEQ.nextval, true, 'Test Guest 2', '$2a$10$iE.0ldRzLk400H.AFXxlA.z3f2jRaudcPJzrQ80JjtGQSI6mhrNiu', 'GUEST', 'TestGuest2');
insert into USER(ID, ENABLED, NAME, PASSWORD, ROLE, USERNAME) values (USERS_SEQ.nextval, true, 'Test Guest 3', '$2a$10$iE.0ldRzLk400H.AFXxlA.z3f2jRaudcPJzrQ80JjtGQSI6mhrNiu', 'GUEST', 'TestGuest3');